'use strict';

process.title = 'OneTwoTrip';

var redis  = require("redis"),
    config = require("./lib/config.js")('default'),
    client = redis.createClient(config.redis),
    Generator = require("./lib/generator.js"),
    Worker = require("./lib/worker.js"),
    Locker = require("./lib/locker.js"),
    log    = require("debug")('OneTwoTrip'),
    argv = require('minimist')(process.argv.slice(2));

var generator = new Generator(client);
var worker    = new Worker(client);
var locker    = new Locker(client);

client.on("error", function (err) {
    console.log("Error while connecting to Redis", err);
    process.exit(1);
});

client.on("ready", function() {
    log("redis is ready!");

    if (argv['getErrors']) {
	worker.getErrorsAndClean(function(err, errors) {
	    if (!err) {
		errors.forEach(function(error) {
		    console.log(error);
		});
		process.exit(0);
	    }
	});
    }
    
    locker.start(500); // locker timeout = generator timeout
    locker.on("master", function() {
	process.title = 'OneTwoTrip Master';
	log("I'm MASTER");
	generator.start(500);
	worker.stop();
    }).on("slave", function() {
	process.title = 'OneTwoTrip Slave';
	log("I'm SLAVE");
	generator.stop();
	worker.start();
	
    });
});
