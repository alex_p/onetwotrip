'use strict';

/**
 * Generator class
 * @module lib/generator
 */

var log = require('debug')('generator');

/**
 * @constructor
 * @param	{Object}	client	redis client instance
 */
function Generator(client) {
    this.client = client;
    this.state  = null;
    this.RUN_STATE  = "run";
    this.STOP_STATE = "stop";
    this.timer   = null;
    this.timeout = null;
    this.cnt     = null;
    this.listName = "list";
}

/**
 * start generator loop
 * @param	{number}	timeout	interval to generate messages
 * @return {Void}
 */
Generator.prototype.start = function(timeout) {
    this.state   = this.RUN_STATE;
    this.timeout = timeout;
    this.timer   = setInterval(this.generate.bind(this), timeout);
}

/**
 * stop generator loop
 * @param {Void}
 * @return {Void}
 */
Generator.prototype.stop = function() {
    this.state = this.STOP_STATE;
    clearInterval(this.timer);
}

/**
 * get message and save to redis
 * @param {Void}
 * @return {Void}
 */
Generator.prototype.generate = function() {
    if (this.state == this.RUN_STATE) {
	this.client.rpush(this.listName, this.getMessage(), function(err, reply){
	    log('generate', err, reply);
	});
    }
}

/**
 * generate new message
 * @param {Void}
 * @return {string} new messages
 */
Generator.prototype.getMessage = function() {
    this.cnt = this.cnt || 0;
    return this.cnt++;
}

module.exports = Generator;
