'use strict';

/**
 * config loader lib
 * @module lib/config
 */

var path = require("path"),
    fs   = require("fs"),
    log  = require("debug")(process.pid + '.config');

/**
 * Get path to config file
 * @param	{string}	file	config name
 * @returns	{string}	relative path to config
 */
function getPath(file) {
    return path.join(__dirname, '../config/' + file + '.json');
}

/**
 * load config
 * @param	{string}	name	Config's filename
 * @return	{*}	Config's JSON
 */
module.exports = function (name) {
    log('use config: %s', name);
    var config = JSON.parse(fs.readFileSync(getPath(name)));

    return config;
}


