'use strict';

/**
 * Worker class
 * @module lib/worker
 */

var multiPopScript = "local result = redis.call('lrange',KEYS[1],0,ARGV[1]-1)\nredis.call('ltrim',KEYS[1],ARGV[1],-1)\nreturn result";

var log = require('debug')('worker'),
    async = require('async');

/**
 * @constructor
 * @param       {Object}	client        redis client instance
 */
function Worker(client) {
    this.client = client;
    this.state  = null;
    this.RUN_STATE  = "run";
    this.STOP_STATE = "stop";
    this.listName = "list";
    this.errListName = "errors";
    this.poolAmount  = 10; // amount of tasks handling at the same time

    this.q = async.queue(function(task, cb) {
	this.eventHandler(task, cb);
    }.bind(this), this.poolAmount);

    // if queue is empty then try to get new jobs
    this.q.drain = function() {
	log('no more items in jobs queue');
	this.getColdStartJobs(this.createTask.bind(this));
    }.bind(this);
}

/**
 * start worker loop
 * @param  {Void}
 * @return {Void}
 */

Worker.prototype.start = function() {
    this.state = this.RUN_STATE;
    this.getColdStartJobs(this.createTask.bind(this));
}

/**
 * stop worker loop
 * @param {Void}
 * @return {Void}
 */
Worker.prototype.stop = function() {
    this.state = this.STOP_STATE;
}

/**
 * create new task[s] in local queue
 * @callback createTask
 * @param {*}	err	error
 * @param {string[]|string}	data	job[s]
 * @return {Void}
 */
Worker.prototype.createTask = function(err, data) {
    var self = this;
    if (err) {
	log('createTask ERR', err);
	process.exit(1);
    }
    
    if (Array.isArray(data)) {
	// here comes jobs from multipop :)
	if (data.length) {
	    data.forEach(function (task) {
		self.q.push(task, self.errorHandler.bind(self));
	    });
	} else {
	    // no jobs in redis
	    // try to get jobs again
	    this.getColdStartJobs(this.createTask.bind(this));
	}
    } else if (data) {
	// create singe task
	this.q.push(data, this.errorHandler.bind(this));
    }
}

/**
 * Get data for worker's queue cold start
 * @param {createTask}	cb	callback to create tasks in local queue
 * @returns {Void}
 */
Worker.prototype.getColdStartJobs = function(cb) {
    if (this.state == this.RUN_STATE) {
	this.client.eval(multiPopScript, 1, this.listName, this.poolAmount, function(err, reply){
	    log('getColdStartJobs', err, reply);
	    cb(err, reply);
	});
    } else if (this.state == this.STOP_STATE) {
	log('stoped from getColdStartJobs');
    }
}

/**
 * Get data for next task
 * @param {createTask}		cb	callback to create task in local queue
 * @return {Void}
 */
Worker.prototype.getNextJob = function(cb) {
    if (this.state == this.RUN_STATE) {
	this.client.lpop(this.listName, function(err, reply){
	    log('getNextJob', err, reply);
	    cb(err, reply);
	});
    } else if (this.state == this.STOP_STATE) {
	log('stoped from getNextJob');
    }
}

/**
 * This function handels tasks
 * @callback {eventHandler}
 * @param {string}	msg	message
 * @param {errorHandler}	cb	callback callback to handle errors
 * @return {Void}
 */
Worker.prototype.eventHandler = function(msg, callback) {
    function onComplete() {
	var error = Math.random() > 0.85;
	callback(error, msg);
    }
    // processing takes time...
    setTimeout(onComplete, Math.floor(Math.random() * 1000));
}

/**
 * This function handels tasks' error
 * @callback {errorHandler}
 * @param {string}	err	error
 * @param {sting}	msg	callback to handle errorsmessage
 * @return {Void}
 */
Worker.prototype.errorHandler = function(err, msg) {
    if (err) {
	// Error in task occured
	log('errorHandler caught error from task:', msg)
	this.client.rpush(this.errListName, msg, function(err, reply){
	    log('errorHandler error logged:', err, reply);
	});
    } else {
	// Task finished successfully
	// Now we can get new data and create one more task.
	log('errorHandler task completed:', msg);
	this.getNextJob(this.createTask.bind(this));
    }
}

/**
 * This function runs when --getErrors option is being called
 * @param {function}	cb	callback to return errors
 * @return {Void}
 */
Worker.prototype.getErrorsAndClean = function(cb) {
    var self = this;
    // get all errors
    this.client.lrange(this.errListName, 0, -1, function(err, reply) {
        if (err) {
	    log("Error while getting errors :)", err);
	    cb(err, null);
        } else {
	    // clean all errors
            self.client.del('errors', function(){
		cb(null, reply);
	    });
        }
    });
}

module.exports = Worker;
