'use strict';

/**
 * Locker class. This class is watching current app state: generator or worker
 * @module lib/locker
 */


var util   = require('util');
var events = require('events');
var log = require('debug')('locker');
var Step = require("step");

var extendScript = 'if redis.call("get", KEYS[1]) == ARGV[1] then return redis.call("pexpire", KEYS[1], ARGV[2]) else return 0 end';
var lockKey = "lock";

/**
 * @constructor
 * @param       {Object}	client        redis client instance
 */
function Locker(client) {
    events.EventEmitter.call(this);
    this.client = client;
    this.value  = null; // we use this "uniq" value to check if we extends own lock
    this.timeout = null;
    this.timer = null;
    this.state = null;
    this.MASTER_STATE = "master";
    this.SLAVE_STATE  = "slave";
}

util.inherits(Locker, events.EventEmitter);

/**
 * start locker's loop
 * @param       {number}	timeout        lock timeout
 * @return {Void}
 */
Locker.prototype.start = function (timeout) {
    var self = this;
    this.timeout = timeout;
    this.timer   = setInterval(self.checkState.bind(self), timeout - 100);
}

/**
 * check current state
 * @param {Void}
 * @return {Void}
 */

Locker.prototype.checkState = function() {
    log("checkState");
    if (this.state != this.MASTER_STATE) {
	// we're not master!
	// try to create new lock
	this._lock();
    } else {
	// we're master!
	// try to extend lock
	this._extend();
	
    }
}

/**
 * try to create lock
 * @param {Void}
 * @fires {Locker#master|Locker#slave}
 * @return {Void}
 */
Locker.prototype._lock = function() {
    var value = this._random();
    var self  = this;
    this.client.set(lockKey, value, 'NX', 'PX', this.timeout, function(err, reply){
	if (err === null && reply === 'OK') {
	    // we're new master
	    self.value = value;
	    log('_lock', "I'M NEW MASTER");
	    if (self.state != self.MASTER_STATE) {
		self.state = self.MASTER_STATE;
		self.emit("master");
	    }
	} else {
	    // we're stil not master
	    self.value = null;
	    log('_lock', "STILL NOT MASTER");
	    if (self.state != self.SLAVE_STATE) {
		self.state = self.SLAVE_STATE;
		self.emit("slave");
	    }
	}
    });
}

/**
 * try to extend existing lock
 * @param {Void}
 * @fires {Locker#master|Locker#slave}
 * @return {Void}
 */
Locker.prototype._extend = function() {
    var self = this;
    this.client.eval(extendScript, 1, lockKey, this.value, this.timeout, function(err, reply){
	if (err === null && reply === 1) {
	    // we're still master
	    log('_extend', 'STILL MASTER');
	    if (self.state != self.MASTER_STATE) {
		self.state = self.MASTER_STATE;
		self.emit("master");
	    }
	} else {
	    // we're no longer master
	    self.value = null;
	    log('_extend', 'NO LONGER MASTER');
	    self.value = null;
	    if (self.state != self.SLAVE_STATE) {
		self.state = self.SLAVE_STATE;
		self.emit("slave");
	    }
	}
    });
}

// @TODO  maybe use ip:pid as the key or something ?
/**
 * generate new random lock value
 * @param {Void}
 * @return {string} ramdom value
 */
Locker.prototype._random = function _random(){
    return Math.random().toString(36).slice(2);
};

module.exports = Locker;